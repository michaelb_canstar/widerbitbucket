// Saves options to chrome.storage
var extensionsGlobal;

function saveOnClickDisplayOption() {
  chrome.storage.sync.set({
    onClickDisplay: document.getElementById("on-click-display").checked
  }, function() {
    showSaveStatus();
    restoreOptions();
  });
}

function saveExtensionsOption(value) {
  chrome.storage.sync.set({
    extensions: value
  }, function() {
    showSaveStatus();
    restoreOptions();
  });
}

function showSaveStatus() {
  // Update status to let user know options were saved.
  var status = document.getElementById("status");
  status.classList.add("visible");
  restoreOptions();
  setTimeout(function() {
    status.classList.remove("visible");
  }, 750);
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restoreOptions() {
  // Use default value onClickDisplay = true, extensions = [].
  chrome.storage.sync.get({
    onClickDisplay: true,
    extensions: []
  }, function(items) {
    document.getElementById("on-click-display").checked = items.onClickDisplay;
    if (items.extensions.length > 0) {
      document.getElementById("extensions-display").innerHTML = items.extensions.join(", ");
    } else {
      document.getElementById("extensions-display").innerHTML = "No extensions hidden.";
    }
    extensionsGlobal = items.extensions;
  });
}

function clearExtensions() {
  saveExtensionsOption([]);
}

function addExtensions() {
  var extensionsToAdd = document.getElementById("extensions").value;
  var updatedExtensions;
  if (extensionsGlobal !== undefined) {
    updatedExtensions = extensionsGlobal.concat(extensionsToAdd.replace(/ /g, "").replace(/\n/g, "").replace(/\./g, "").split(","));
  } else {
    updatedExtensions = extensionsToAdd.replace(/ /g, "").split(",");
  }

  // Only keep extensions that are not blank and that are unique
  var sortedUniqueExtensions = [];
  for (var i = 0; i < updatedExtensions.length; i++) {
    extension = updatedExtensions[i];
    if (extension !== "" && sortedUniqueExtensions.indexOf(extension) === -1) {
      sortedUniqueExtensions.push(extension);
    }
  }
  sortedUniqueExtensions.sort();

  saveExtensionsOption(sortedUniqueExtensions);
  document.getElementById("extensions").value = null;
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("on-click-display").addEventListener("click", saveOnClickDisplayOption);
document.getElementById("clear-extensions").addEventListener("click", clearExtensions);
document.getElementById("add-extensions").addEventListener("click", addExtensions);
