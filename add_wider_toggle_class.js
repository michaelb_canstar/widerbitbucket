chrome.storage.sync.get({
  wider: true
}, function(items) {
  wider = items.wider;

  var panel = document.getElementsByClassName("aui-page-panel-inner")[0];
  if (wider) {
    panel.classList.add("wider-toggle");
  } else {
    panel.classList.remove("wider-toggle");
  }
});
