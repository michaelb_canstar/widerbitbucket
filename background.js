var betterTabs = {};

chrome.browserAction.onClicked.addListener(function(tab){
  chrome.tabs.executeScript(null, {file: "add_display_diff_toggle_class.js"});
  chrome.tabs.executeScript(null, {file: "add_on_click_display_toggle.js"});

  var bitbucketRegex = new RegExp(/https:\/\/bitbucket.org\//);
  var bitbucketMatch = bitbucketRegex.exec(tab.url);
  if ((bitbucketMatch) && tab.status === 'complete' && !betterTabs[tab.id]) {
    chrome.tabs.insertCSS(tab.id, {
      file: "better.css" });
    betterTabs[tab.id] = true;
    showBadge();
  } else {
    chrome.tabs.insertCSS(tab.id, {
      file: "regular.css" });
    betterTabs[tab.id] = false;
    hideBadge();
  }
});

chrome.tabs.onActivated.addListener(function(activeInfo) {
  if (betterTabs[activeInfo.tabId]) {
    showBadge();
  } else {
    hideBadge();
  }
});

function showBadge() {
  chrome.browserAction.setBadgeBackgroundColor({color:[190, 0, 0, 230]});
  chrome.browserAction.setBadgeText({text:"B"});
}

function hideBadge() {
  chrome.browserAction.setBadgeBackgroundColor({color:[0, 0, 0, 0]});
  chrome.browserAction.setBadgeText({text:""});
}
