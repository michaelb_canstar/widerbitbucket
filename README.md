# Better Bitbucket (formerly Wider Bitbucket)

Better Bitbucket is a Chrome extension that allows you to choose which file extensions you would like to hide when looking at a pull request.  You can also show/hide the diff of one file by clicking on the header containing its filename.

This extension is not affiliated with Bitbucket.

This is derived from the original [Wider Github][1] extension by tmlee.

## Installation & Usage

1. Download this [repo][2]
2. Open Google Chrome Browser
3. Open Extensions
4. Select 'Load unpacked extension'
5. Open the directory containing this repo
6. Browse to Bitbucket code and click the extension icon

[1]: https://github.com/tmlee/wider_github
[2]: https://bitbucket.org/raymondhytang/widerbitbucket/downloads/widerbitbucket.zip

## Versions

### 0.1.6
- Remove wider option because Bitbucket's new layout uses the full width of the window
- Hide diffs that are too big to render on page load

### 0.1.5
- Add option to click on the header of a file's diff container to show/hide the diff

### 0.1.4
- Set default value of wider flag to true

### 0.1.3
- Add options page to choose whether to use a wider width and to choose what file extensions to hide
- Add icon badge to indicate whether the extension is active in the current tab

### 0.1.2
- Hide .meta files for Unity projects

### 0.1.1
- Add transparency to the extension icon

### 0.1.0
- Widen the diff container in Bitbucket to 90% of the Chrome window width
