chrome.storage.sync.get({
  onClickDisplay: true
}, function(items) {
  if (items.onClickDisplay) {
    var diffContainers = document.getElementsByClassName("diff-container");
    for (var i = 0; i < diffContainers.length; i++) {
      var diffContainer = diffContainers[i];
      diffContainer.getElementsByClassName("heading")[0].onclick = function() {
        var contentContainer = this.parentNode.getElementsByClassName("content-container")[0];
        if (contentContainer !== undefined) {
          var contentContainerDiff = contentContainer.getElementsByClassName("diff")[0];
          if (contentContainerDiff.style.display === "none") {
            contentContainerDiff.style.display = "";
          } else {
            contentContainerDiff.style.display = "none";
          }
        }

        var refractContainer = this.parentNode.getElementsByClassName("refract-container")[0];
        if (refractContainer !== undefined) {
          var refractContainerDiff = refractContainer.getElementsByClassName("refract-content-container")[0];
          if (refractContainerDiff.style.display === "none") {
            refractContainerDiff.style.display = "";
          } else {
            refractContainerDiff.style.display = "none";
          }
        }
      };
    }
  } else {
    var diffContainers = document.getElementsByClassName("diff-container");
    for (var i = 0; i < diffContainers.length; i++) {
      var diffContainer = diffContainers[i];
      diffContainer.getElementsByClassName("heading")[0].onclick = function() {
      };
    }
  }
});
