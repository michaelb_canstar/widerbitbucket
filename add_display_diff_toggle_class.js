// This adds the display-diff-toggle class to the commentable-diff element of files with file extensions specified in the extension options so it can be hidden by the CSS

chrome.storage.sync.get({
  extensions: ""
}, function(items) {
  extensionsArray = items.extensions;

  var diffContainers = document.getElementsByClassName("bb-udiff");
  for (var i = 0; i < diffContainers.length; i++) {
    var diffContainer = diffContainers[i];
    var filename = diffContainer.getElementsByTagName("h1")[0].innerHTML;

    diffContainer.classList.remove("display-diff-toggle");

    var hasExtension = false;
    for (var j = 0; j < extensionsArray.length; j++) {
      if (filename.lastIndexOf(".") !== -1 && filename.lastIndexOf("." + extensionsArray[j]) === filename.lastIndexOf(".")) {
        hasExtension = true;
        break;
      }
    }

    if (!hasExtension) {
      continue;
    }

    diffContainer.classList.add("display-diff-toggle");
  }
});
